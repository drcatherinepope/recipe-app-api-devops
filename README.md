# Recipe App API Project

This is my project from  [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/) course.

The course teaches how to build a REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development
 - Terraform

## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000
